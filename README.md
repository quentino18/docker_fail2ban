# Docker fail2ban

This docker set up Fail2ban
This allows to prevent bruteforce attacks.
The log file at /var/log/fail2ban.log must be created first with correct rights

Use 'banaction = nftables-input' for Host jails
Use 'banaction = nftables-forward' for Containers jails

Change 'port = ssh' to your custom ssh port if you have one 

#### Create and run home docker as detached
    docker-compose up -d

#### Create and run home docker
    docker-compose up

#### Stop home docker and related network
    docker-compose down

#### Access to logs and follow
    docker-compose logs -f

#### Pull image
    docker-compose pull

#### Open terminal on docker container
    docker containerid exec -it dockname /bin/bash
